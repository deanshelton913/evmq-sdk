import { RootLogger } from "loglevel";
import { getLogger } from "./logger";
import { getMakeRequest, makeRequest as makeRequestType } from "./makeRequest";
import IsomorphicWebSocket from "isomorphic-ws";

export class EvmQueueSdk {
  public domain: string;
  public websocketProtocol: string;
  public httpProtocol: string;
  public makeRequest: typeof makeRequestType;
  public logger: RootLogger;
  public idToken: string;
  public accessToken: string;
  public apiKey: string;
  public unsafeLogUnRedactedResponses: boolean;
  public withCredentials: boolean;
  public unacknowledgedConsumerPings = 0;
  public unacknowledgedQueueDetailPings = 0;

  constructor({
    domain,
    websocketProtocol,
    httpProtocol,
    logger,
    unsafeLogUnRedactedResponses,
    apiKey,
    idToken,
    accessToken,
    withCredentials,
  }: {
    domain?: string;
    logger?: RootLogger;
    websocketProtocol?: string;
    httpProtocol?: string;
    unsafeLogUnRedactedResponses?: boolean;
    apiKey?: string;
    idToken?: string;
    accessToken?: string;
    withCredentials?: boolean;
  }) {
    this.domain = domain || "evmqueue.com";
    this.websocketProtocol = websocketProtocol || "wss";
    this.httpProtocol = httpProtocol || "https";
    this.unsafeLogUnRedactedResponses = unsafeLogUnRedactedResponses || false;
    this.withCredentials =
      withCredentials === undefined ? true : withCredentials;
    this.logger = logger || getLogger({ silent: false });
    this.apiKey = apiKey;
    this.idToken = idToken;
    this.accessToken = accessToken;
    this.makeRequest = getMakeRequest(this);
  }

  public setWithCredentials(withCredentials: boolean) {
    this.withCredentials = withCredentials; // Turn off the saving of cookies completely.
    this.makeRequest = getMakeRequest(this);
  }

  public setIdToken({ idToken }: { idToken: string }) {
    this.idToken = idToken;
  }

  public setAccessToken({ accessToken }: { accessToken: string }) {
    this.accessToken = accessToken;
  }

  public setApiKey({ apiKey }: { apiKey: string }) {
    this.apiKey = apiKey;
  }

  public async login({
    email,
    password,
    rememberMe,
  }: {
    email: string;
    password: string;
    rememberMe?: boolean;
  }) {
    return await this.makeRequest<LoginResponse & { error: string }>({
      method: "POST",
      url: "/v1/accounts/login",
      data: {
        email,
        password,
        rememberMe: rememberMe === undefined ? true : Boolean(rememberMe),
      },
    });
  }

  public get headers() {
    if (!this.apiKey) return undefined;
    return {
      ...(this.apiKey && { authorization: `Bearer ${this.apiKey}` }),
    };
  }

  public async logout() {
    return await this.makeRequest<{ data: "OK" } & { error?: string }>({
      method: "DELETE",
      url: "/v1/accounts/logout",
      headers: this.headers,
    });
  }

  public async myAccount() {
    return await this.makeRequest<{ email: "string" } & { error?: string }>({
      method: "GET",
      url: "/v1/accounts/me",
      headers: this.headers,
    });
  }

  public async attachPaymentMethod({
    paymentMethodId,
  }: {
    paymentMethodId: string;
  }) {
    return await this.makeRequest<{ data: "OK" } & { error?: string }>({
      method: "PUT",
      url: "/v1/billing/payment-methods/attach",
      headers: this.headers,
      data: {
        paymentMethodId,
      },
    });
  }

  public async listPaymentMethods() {
    return await this.makeRequest<{ data: PaymentMethod[] } & { error?: string }>({
      method: "GET",
      url: "/v1/billing/payment-methods",
      headers: this.headers,
    });
  }

  public async deletePaymentMethod(paymentMethodId: string) {
    return await this.makeRequest<{ data: "OK" } & { error?: string }>({
      method: "DELETE",
      url: `/v1/billing/payment-methods/${paymentMethodId}`,
      headers: this.headers,
    });
  }

  public async register({
    email,
    password,
  }: {
    email: string;
    password: string;
  }) {
    return await this.makeRequest<RegistrationResponse>({
      method: "POST",
      url: "/v1/accounts",
      data: {
        email,
        password,
      },
    });
  }

  public async verify({
    email,
    confirmationCode,
  }: {
    email: string;
    confirmationCode: string;
  }) {
    return await this.makeRequest<any>({
      method: "POST",
      url: "/v1/accounts/verify",
      data: {
        email,
        confirmationCode,
      },
    });
  }

  public async forgotPassword({ email }: { email: string }) {
    return await this.makeRequest<any>({
      method: "POST",
      url: "/v1/accounts/forgot-password",
      data: {
        email,
      },
    });
  }

  public async forgotPasswordVerify({
    email,
    password,
    confirmationCode,
  }: {
    email: string;
    password: string;
    confirmationCode: string;
  }) {
    return await this.makeRequest<any>({
      method: "POST",
      url: "/v1/accounts/forgot-password/verify",
      data: {
        email,
        password,
        confirmationCode,
      },
    });
  }

  public async verifyResend({ email }: { email: string }) {
    return await this.makeRequest<{ data: "OK"; error?: string }>({
      method: "POST",
      url: "/v1/accounts/verify-resend",
      data: {
        email,
      },
    });
  }

  public async apiKeyCreate(data: ApiKeyCreateParams) {
    return await this.makeRequest<ApiKeyCreateResponse>({
      method: "POST",
      url: "/v1/api-keys",
      headers: this.headers,
      data,
    });
  }

  public async apiKeyShow(data: ApiKeyShowParams) {
    return await this.makeRequest<ApiKeyShowResponse>({
      method: "GET",
      url: `/v1/api-keys/${data.id}`,
      headers: this.headers,
      data,
    });
  }

  public async apiKeyUpdate(data: ApiKeyEditParams) {
    return await this.makeRequest<ApiKeyUpdateResponse>({
      method: "PUT",
      url: `/v1/api-keys/${data.id}`,
      headers: this.headers,
      data,
    });
  }

  public async apiKeyList() {
    return await this.makeRequest<ApiKeyListResponse>({
      method: "GET",
      url: "/v1/api-keys",
      headers: this.headers,
    });
  }

  public async apiKeyDelete({ id }: { id: string }) {
    return await this.makeRequest<ApiKeyListResponse>({
      method: "DELETE",
      url: `/v1/api-keys/${id}`,
      headers: this.headers,
    });
  }

  public async useRefreshTokenToGetNewIdToken() {
    return await this.makeRequest<UseRefreshTokenResponse>({
      method: "GET",
      url: `/v1/accounts/login/refresh`,
    });
  }

  public async jobCreate({
    name,
    description,
    startBlock,
    endBlock,
    eventFilter,
    maxScaleLimit,
    rpcUrl,
    spendingLimit,
    chainHeadBuffer,
    maxLogSpanInBlocks,
    maxWorkerBlockSpan,
  }: JobCreateParams) {
    return await this.makeRequest<JobCreateResponse>({
      method: "POST",
      url: `/v1/jobs`,
      data: {
        name,
        description,
        rpcUrl,
        startBlock,
        endBlock,
        eventFilter,
        chainHeadBuffer,
        maxLogSpanInBlocks,
        maxWorkerBlockSpan,
        maxScaleLimit,
        spendingLimit,
      },
    });
  }

  public async jobUpdate({
    id,
    name,
    description,
    startBlock,
    endBlock,
    eventFilter,
    maxScaleLimit,
    rpcUrl,
    spendingLimit,
    chainHeadBuffer,
    maxLogSpanInBlocks,
    maxWorkerBlockSpan,
  }: JobCreateParams & { id: string }) {
    return await this.makeRequest<JobCreateResponse>({
      method: "PUT",
      url: `/v1/jobs/${id}`,
      data: {
        name,
        description,
        rpcUrl,
        startBlock,
        endBlock,
        eventFilter,
        chainHeadBuffer,
        maxLogSpanInBlocks,
        maxWorkerBlockSpan,
        maxScaleLimit,
        spendingLimit,
      },
    });
  }

  public async jobList({ pageSize, pageNumber, jobStatus }) {
    const s = new URLSearchParams();
    if (pageSize !== undefined) s.append("page_size", pageSize);
    if (pageNumber !== undefined) s.append("page", pageNumber);
    if (jobStatus) s.append("status", jobStatus);

    return await this.makeRequest<JobListResponse>({
      method: "GET",
      url: `/v1/jobs${s.toString() ? "?" + s.toString() : ""}`,
      headers: this.headers,
    });
  }

  public async jobShow({ id }) {
    return await this.makeRequest<JobShowResponse>({
      method: "GET",
      url: `/v1/jobs/${id}`,
      headers: this.headers,
    });
  }

  public async jobCancel({ id }) {
    return await this.makeRequest<JobShowResponse>({
      method: "DELETE",
      url: `/v1/jobs/${id}`,
      headers: this.headers,
    });
  }

  public async jobRead({ id, take = 1 }) {
    return await this.makeRequest<JobShowResponse>({
      method: "GET",
      url: `/v1/jobs/${id}/read-queue?take=${take}`,
      headers: this.headers,
    });
  }

  /**
   *
   */
  private getAck = (deliveryTag, ws) => async () => {
    ws.send(
      JSON.stringify({
        routingKey: "ACK",
        body: { deliveryTag },
      })
    );
  };

  public wsJobQueueDetails({
    jobId,
    onMessage,
    onError,
    onOpen,
    onClose,
  }: {
    jobId: string;
    onMessage?: (payload: any) => Promise<void>;
    onError?: () => Promise<void>;
    onOpen?: () => Promise<void>;
    onClose?: () => Promise<void>;
  }): IsomorphicWebSocket {
    const url = `${this.websocketProtocol}://${this.domain}/v1/jobs/${jobId}/ws`;
    console.log(
      `Websocket Connect: ${url}, ${JSON.stringify({ headers: this.headers })}`
    );
    const ws: IsomorphicWebSocket = new IsomorphicWebSocket(url);
    let pingInterval = setInterval(() => {
      if (this.unacknowledgedQueueDetailPings > 3) {
        console.warn("evmq - unacknowledged pings > 3. Closing.");
        ws.close(3000, "unacknowledged pings > 3");
        clearInterval(pingInterval);
      }
      this.unacknowledgedQueueDetailPings += 1;
      ws.send(JSON.stringify({ routingKey: "PING" }));
    }, 10000);
    ws.onerror = () => {
      clearInterval(pingInterval);
      onError();
    };
    ws.onopen = onOpen;
    ws.onclose = () => {
      clearInterval(pingInterval);
      onClose();
    };
    ws.onmessage = async (msg: any) => {
      const payload = JSON.parse(msg.data);
      if (payload.routingKey === "PONG") {
        console.log(
          "evmq - unacknowledged pings",
          this.unacknowledgedQueueDetailPings
        );
        this.unacknowledgedQueueDetailPings -= 1;
      }
      if (payload.routingKey === "QUEUE_DETAIL") {
        onMessage(payload);
      }
    };
    return ws;
  }

  /**
   *
   */
  public wsJobQueueConsumer({
    jobId,
    onMessage,
    onError,
    onOpen,
    onClose,
  }: {
    jobId: string;
    onMessage?: (
      payload: LogConsumerMessage,
      ack: () => Promise<void>
    ) => Promise<void>;
    onError?: () => Promise<void>;
    onOpen?: () => Promise<void>;
    onClose?: () => Promise<void>;
  }): IsomorphicWebSocket {
    const url = `${this.websocketProtocol}://${this.domain}/v1/jobs/${jobId}/ws/consume`;
    const ws = new IsomorphicWebSocket(url);
    let pingInterval = setInterval(() => {
      if (this.unacknowledgedConsumerPings > 3) {
        console.warn("evmq - unacknowledged pings > 3. Closing.");
        ws.close(3000, "unacknowledged pings > 3");
        clearInterval(pingInterval);
      }
      this.unacknowledgedConsumerPings += 1;
      ws.send(JSON.stringify({ routingKey: "PING" }));
    }, 10000);

    const boundOnMessageHandler = async (
      message: IsomorphicWebSocket.MessageEvent
    ) => {
      const payload = JSON.parse(message.data.toString()) as LogConsumerMessage;

      if (payload.routingKey === "PONG") {
        this.unacknowledgedConsumerPings -= 1;
      }
      if (payload.routingKey === "LOG_DATA") {
        const ack = this.getAck(payload.deliveryTag, ws);
        onMessage(payload, ack);
      }
    };
    ws.onerror = onError;
    ws.onopen = onOpen;
    ws.onclose = onClose;
    ws.onmessage = boundOnMessageHandler;
    return ws;
  }
}

export interface PaymentMethod {
  id: string;
  fingerprint: string;
  brand: string;
  last4: string;
  created: number;
  liveMode: boolean;
  type: string;
}

export interface ApiKeyCreateParams {
  name: string;
  description: string;
}

export interface ApiKeyShowParams {
  id: string;
}

export interface ApiKeyEditParams {
  id: string;
  name?: string;
  description?: string;
}

export interface RegistrationResponse {
  error?: string;
  data: {
    UserConfirmed: boolean;
    UserSub: string;
    CodeDeliveryDetails: {
      AttributeName: string;
      DeliveryMedium: string;
      Destination: string;
    };
    ResultMetadata: {};
  };
}

export interface LoginResponse {
  error?: string;
  data: {
    AuthenticationResult: {
      AccessToken: string;
      ExpiresIn: number;
      IdToken: string;
      NewDeviceMetadata?: null;
      RefreshToken: string;
      TokenType: string;
    };
    ChallengeName: string;
    ChallengeParameters: {};
    Session?: null;
    ResultMetadata: {};
  };
}

export interface LogConsumerMessage {
  routingKey: string;
  deliveryTag: number;
  body: {
    blockNumber: number;
    blockHash: string;
    transactionIndex: number;
    removed: boolean;
    address: string;
    data: string;
    topics: Array<string>;
    transactionHash: string;
    logIndex: number;
  };
  contentType: string;
  deduplicationId: string;
}

export interface ApiKeyCreateResponse {
  error?: string;
  data: {
    apiKeyId: string;
    apiKey: string;
    rateLimitPerMin: number;
  };
}

export interface ApiKeyShowResponse {
  error?: string;
  data: {
    id: string;
    name: string;
    description: string;
  };
}

export interface ApiKeyUpdateResponse {
  error?: string;
  data: {
    id: string;
    name: string;
    description: string;
  };
}
export interface ApiKeyListResponse {
  error?: string;
  data: {
    apiKeys: {
      id: string;
      name: string;
      description: string;
    }[];
  };
}

export interface UseRefreshTokenResponse {
  error?: string;
  data: {
    AccessToken: string;
    ExpiresIn: number;
    IdToken: string;
    NewDeviceMetadata: unknown;
    RefreshToken: null;
    TokenType: "Bearer";
  };
}

export interface JobCreateResponse {
  error?: string;
  data: Job;
}

export interface JobListResponse {
  error?: string;
  data: Job[];
}

export interface JobShowResponse {
  error?: string;
  data: {
    job: Job;
    approximateQueueLength: number;
    workerStatus: [{ status: string; count: 0 }];
  };
}

export interface Job {
  CreatedAt: string;
  UpdatedAt: string;
  DeletedAt: string;
  ID: string;
  name: string;
  description: string;
  rpcUrl: string;
  startBlock: number;
  endBlock: number;
  eventFilter: string;
  chainHeadBuffer: number;
  maxLogSpanInBlocks: number;
  spendingLimit: number;
  maxWorkerBlockSpan: number;
  maxScaleLimit: number;
  cancelledAt: string;
  blocksProcessed: number;
  highestBlockAssignment: number;
  logsFound: number;
  status: string;
  chainId: string;
}
export interface JobCreateParams {
  name: string;
  description?: string;
  startBlock: number;
  endBlock: number;
  eventFilter: string;
  maxScaleLimit: number;
  rpcUrl: string;
  spendingLimit: number;
  chainHeadBuffer: number;
  maxLogSpanInBlocks: number;
  maxWorkerBlockSpan: number;
}
