import axios, {AxiosResponse, AxiosError, AxiosRequestConfig} from 'axios';
import Redactyl from 'redactyl.js'
import { FailureByDesign } from './FailureByDesign';
import { getLogger } from './logger';
import { EvmQueueSdk } from './';

const silentLogger = getLogger({ silent: true });
const baseAxios = axios.create({ withCredentials: true })
let id = 0;
let redactyl = new Redactyl({
  'properties': [ 'apiKey', 'password', 'AccessToken', 'RefreshToken', 'IdToken' ]
});

export async function makeRequest<T>(config:AxiosRequestConfig, logger=silentLogger, axiosInstance=baseAxios, unsafeLogUnRedactedResponses=false) {
  id+=1;
  const base = config.baseURL || axiosInstance.defaults.baseURL
  let result: AxiosResponse<T&{error:string}>
  try {
    logger.info(`[${id}] ${config.method} > \t| ${base}${config.url}`);
    result = await axiosInstance({baseURL: axiosInstance.defaults.baseURL, ...config});
    logger.info(`[${id}] ${config.method} < ${result.status}\t| ${base}${config.url}`);
  } catch (e) {
    // This will catch any HTTP errors, or network issues.
    const err = e as AxiosError;
    logger.info(`[${id}] ${err?.response?.status} ${config.method} < ${base}${config.url}`);
    err?.response?.data && logger.error(`Response Body: ${JSON.stringify(err?.response?.data)}`);
    throw new FailureByDesign(
      'SUBSYSTEM_ERROR',
      JSON.stringify(err?.response?.data) || e.message
    );
  }
  // This will catch any exceptions which leave the application unable to continue processing the response.
  const redactedData = unsafeLogUnRedactedResponses === true ? result?.data : redactyl.redact(result?.data as any);
  logger.info(`[${id}] ${config.method} <    \t| ${JSON.stringify(redactedData)}`)
  if(result.status === 403) {
    throw new FailureByDesign('FORBIDDEN', result?.data?.error)
  }
  if(result.status.toString().startsWith('5')){
    const diagnosticInfo = { method: config.method, url: config.url, status:result.status, resultBody: JSON.stringify(redactedData) };
    throw new FailureByDesign('UNEXPECTED', "Please submit a support ticket.", diagnosticInfo)
  }
  return result;
}


export function getMakeRequest(sdk: EvmQueueSdk){
  return <T>(config:AxiosRequestConfig) => {
    const axiosInstance = axios.create({ 
      baseURL: `${sdk.httpProtocol}://${sdk.domain}`, 
      validateStatus: () => true, 
      withCredentials: sdk.withCredentials 
    })
    let headers = {}  
    if(sdk.apiKey) {
      headers = {
        authorization: `Bearer ${sdk.apiKey}`
      }
    }
    if(sdk.idToken) {
      headers = {
        authentication: sdk.idToken
      }
    }
    config.headers = headers;
    return makeRequest<T>(config, sdk.logger, axiosInstance, sdk.unsafeLogUnRedactedResponses)
  }
}